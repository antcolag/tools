Tools
===

This module provides a set of functions to build the injectable interface
generators such the reactive, readable and observable interfaces.

The injectProperties function
---

### Example

```javascript
var myobj = {}

injectProperties.call(myobj, {
	aNewMethod(){ /* do something */}
})

// myobj now have a non iterable 'aNewMethod' method
myobj.aNewMethod()
```
